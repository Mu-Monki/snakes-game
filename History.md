### v0.1
    * Made the main file of the game
    * Created the game screen
    
### v0.2
    * Completed the base game
    * Has score functionality
    * Added basic start screen
    
### V0.3
    * Changed color values